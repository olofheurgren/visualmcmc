package se.cbb.vmcmc.main;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.apache.commons.math3.stat.inference.MannWhitneyUTest;

import se.cbb.vmcmc.libs.MCMCDataContainer;
import se.cbb.vmcmc.libs.MCMCFileReader;
import se.cbb.vmcmc.libs.MCMCMath;
import se.cbb.vmcmc.libs.MCMCTree;
import se.cbb.vmcmc.libs.MCMCTreeParamTesting;
import se.cbb.vmcmc.libs.SequenceFileWriter;

public class MCMCCommandLine {
	public static void commandline (int choice, String file1, String file2, 
			int burnin, double confidencelevel, String path, String outFile, 
			Double alpha, int burnin1, int burnin2) throws Exception {
		/* ******************** FUNCTION VARIABLES ******************************** */
		File 						file;
		int 						numSeries;
		int 						serieLength;
		int 						ess;
		int 						geweke;
		int 						numTreeSeries;
		int 						overallConvergenceGeweke;
		int 						overallConvergenceESS;
		int 						overallConvergenceGR;
		MCMCDataContainer 			datacontainer;
		ArrayList<String> 			numSeriesArray;
		boolean 					gelmanRubin;
		Object[] 					serie;
		final NumberFormat 			formatter = new DecimalFormat("0.00");

		/* ******************** VARIABLE INITIALIZERS ***************************** */
		file = new File(path + file1);
		datacontainer = null;
		overallConvergenceGeweke = -2;
		overallConvergenceESS = -2;
		overallConvergenceGR = -2;
		serieLength = 0;

		/* ******************** FUNCTION BODY ************************************* */
		if (confidencelevel < 0)
			confidencelevel = 0;
		else if(confidencelevel > 100)
			confidencelevel = 100;

		//System.err.println("VMCMC is computing results ... Please wait ...");
		if(file != null && file2.equals("")) {
			datacontainer = MCMCFileReader.readMCMCFile(file, false, false);	
			int standardizedESS = MCMCMath.calculateStandardizedESS(datacontainer);
			int normalizedESS = MCMCMath.calculateNormalizedESS(datacontainer);
			burnin = calculateBurnin(burnin, datacontainer);

			if(datacontainer != null) {
				if(choice < 10) {
					numSeries = datacontainer.getNumValueSeries();

					if(numSeries != 0) {
						numSeriesArray = datacontainer.getValueNames();

						if(choice < 9) {
							MCMCCommandLine.printLine(path, outFile, "{\n\t\"File\": \"" + file1 + "\",");
							MCMCCommandLine.printLine(path, outFile, "\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
							MCMCCommandLine.printLine(path, outFile, "\t\"Burnin\": " + burnin + ",");
							if (choice != 8) { //Dont print '"Parameters": {' for --convergencetest
								MCMCCommandLine.printLine(path, outFile, "\t\"Parameters\": {");
							}
						}
						int pos = 0;
						for (List<Double> valueSerie : datacontainer.getValueSeries()) {
							serie = valueSerie.toArray();
							serieLength	= serie.length;

							if(serieLength < 100){
								MCMCCommandLine.printLine(path, outFile, "\t\tSmall dataset. Statistics and tests can not be computed.\n\t}\n}");
								System.exit(0);
							}

							if(choice < 8) 
								MCMCCommandLine.printLine(path, outFile, "\t\t\"" + numSeriesArray.get(pos) + "\": " + "{");

							if(choice == 5) {
								ess = MCMCMath.calculateESSmax(serie);
								MCMCCommandLine.printLine(path, outFile, "\t\t\t\"ESS\": {\n\t\t\t\t\"Status\": \"Converged\",");
								MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + ess + "\n\t\t\t}");
							} else if(choice == 4) {
								geweke = MCMCMath.calculateGeweke(serie);
								if (geweke != -1) {
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geweke\": {\n\t\t\t\t\"Status\": \"Converged\",");
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + geweke + "\n\t\t\t}");							
								} else 
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geweke\": \"Not converged\"");
							} else if(choice == 6) {
								gelmanRubin = MCMCMath.GelmanRubinTest(serie, burnin);
								if(gelmanRubin == true)	{
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \"Converged\",");
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + burnin + "\n\t\t\t}");
								} else {
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \"Not Converged\",");
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + burnin + "\n\t\t\t}");
								}
							} 
							
							if(choice == 2 || choice == 7) {
								geweke = MCMCMath.calculateGeweke(serie);
								ess = MCMCMath.calculateESSmax(serie);

								MCMCCommandLine.printLine(path, outFile, "\t\t\t\"ESS\": {\n\t\t\t\t\"Status\": \"Converged\",");
								MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + ess + "\n\t\t\t},");
								if(ess > overallConvergenceESS) {
									overallConvergenceESS = ess;
								}

								if (geweke != -1) {
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geweke\": {\n\t\t\t\t\"Status\": \"Converged\",");
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + geweke + "\n\t\t\t},");	
									if(overallConvergenceGeweke != -1 && geweke > overallConvergenceGeweke) {
										overallConvergenceGeweke = geweke;
									}
								} else {
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geweke\": \"Not converged\"");
									overallConvergenceGeweke = -1;
								}

								int originalBurnin = 0;
								gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
								while(gelmanRubin != true && originalBurnin < (0.5 * serie.length)) {
									originalBurnin = originalBurnin + 1;
									gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
								}
								if(gelmanRubin == true)	{
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \"Converged\",");
									if(choice != 7)
										MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t}");
									else
										MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t},");
									if(overallConvergenceGR != -1 && originalBurnin > overallConvergenceGR)
										overallConvergenceGR = originalBurnin;
								} else {
									MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Gelman_Rubin\": {\n\t\t\t\t\"Status\": \" Not Converged\",");
									if(choice != 7)
										MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t}");
									else
										MCMCCommandLine.printLine(path, outFile, "\t\t\t\t\"Burn_in\": " + originalBurnin + "\n\t\t\t},");
									overallConvergenceGR = -1;
								}
							}

							if(choice == 8 || choice == 9) {
								geweke = MCMCMath.calculateGeweke(serie);
								ess = MCMCMath.calculateESSmax(serie);

								if(overallConvergenceESS != -1 && ess > overallConvergenceESS) 
									overallConvergenceESS = ess;
								
								if (geweke != -1) {
									if(overallConvergenceGeweke != -1 && geweke > overallConvergenceGeweke) 
										overallConvergenceGeweke = geweke;		
								} else
									overallConvergenceGeweke = -1;
								int originalBurnin = 0;
								gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
								while(gelmanRubin != true && originalBurnin < (0.5 * serie.length)) {
									originalBurnin = originalBurnin + 1;
									gelmanRubin	= MCMCMath.GelmanRubinTest(serie, originalBurnin);
								}
								if(gelmanRubin == true)	{
									if(overallConvergenceGR != -1 && originalBurnin > overallConvergenceGR)
										overallConvergenceGR = originalBurnin;
								} else
									overallConvergenceGR = -1;
							}

							if(serie.length - burnin > 0 && (choice == 3 || choice == 7)) {
								ComputeAndPrintStatistics(burnin, serie, path, outFile, confidencelevel);
							}
							if(choice < 8)
								MCMCCommandLine.printString(path, outFile, "\t\t}");
							if(pos < numSeries - 1 && choice < 8)
								MCMCCommandLine.printLine(path, outFile, ",");
							pos++;
						}
						if (choice >= 2 && choice <= 6) { //close '"Parameters": {'
							MCMCCommandLine.printLine(path, outFile, "\n\t}");
						}
					} 
					if(choice == 8) {						
						MCMCCommandLine.printLine(path, outFile, "\t\"Convergence_Test_Results\": {");
						if(overallConvergenceGeweke == -1) 
							MCMCCommandLine.printLine(path, outFile, "\t\t\"Geweke\": \"All parameters did not converge.\",");
						else 
							MCMCCommandLine.printLine(path, outFile, "\t\t\"Geweke\": " + overallConvergenceGeweke + ",");
						if(overallConvergenceESS == -1) 
							MCMCCommandLine.printLine(path, outFile, "\t\t\"ESS\": \"All parameters did not converge.\",");
						else 
							MCMCCommandLine.printLine(path, outFile, "\t\t\"ESS\": " + overallConvergenceESS + ",");
						if(overallConvergenceGR == -1) 
							MCMCCommandLine.printLine(path, outFile, "\t\t\"Gelman_Rubin\": \"All parameters did not converge.\",");
						else
							MCMCCommandLine.printLine(path, outFile, "\t\t\"Gelman_Rubin\": " + overallConvergenceGR + ",");
						MCMCCommandLine.printLine(path, outFile, "\t\t\"Standardized ESS\": " + standardizedESS + ",");
						MCMCCommandLine.printLine(path, outFile, "\t\t\"Normalized ESS\": " + normalizedESS);
						MCMCCommandLine.printLine(path, outFile, "\t}");	
					}

					if(choice == 9) {
						MCMCCommandLine.printLine(path, outFile, "{\n\t\"File\": \"" + file1 + "\",");
						MCMCCommandLine.printLine(path, outFile, "\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
						MCMCCommandLine.printLine(path, outFile, "\t\"Burnin\": " + burnin + ",");
						if(overallConvergenceESS > -1) {
							int rand = overallConvergenceESS + (int)(Math.random() * (serieLength - overallConvergenceESS + 1));
							MCMCCommandLine.printLine(path, outFile, "\t\"Sample\": " + rand + ",");
							MCMCCommandLine.printLine(path, outFile, "\t\"Parameters\": {");
							numSeriesArray = datacontainer.getValueNames();
							ArrayList<String> treeSeriesNames = datacontainer.getTreeNames();

							int pos = 0;
							for (List<Double> i : datacontainer.getValueSeries()) {
								MCMCCommandLine.printString(path, outFile, "\t\t\"" + numSeriesArray.get(pos) + "\" : " + i.get(rand - 1));
								if(pos < numSeries - 1)
									MCMCCommandLine.printLine(path, outFile, ",");
								pos++;
							}
							if(treeSeriesNames.size() != 0)
								MCMCCommandLine.printString(path, outFile, ",");
							MCMCCommandLine.printLine(path, outFile, "");

							pos = 0;
							for (ArrayList<MCMCTree> i : datacontainer.getTreeSeries()) {
								String newtree = new String(i.get(rand - 1).getData());
								MCMCCommandLine.printString(path, outFile, "\t\t\"" + treeSeriesNames.get(pos) + "\" : \"" + newtree + "\"");
								if(pos < treeSeriesNames.size() -1)
									MCMCCommandLine.printLine(path, outFile, ",");
								pos++;
							}
							MCMCCommandLine.printLine(path, outFile, "\t}");
						}
						else {
							MCMCCommandLine.printLine(path, outFile, "\t\"Sample\" : \"Not converged\"");
						}
					}
				} else {
					numTreeSeries = datacontainer.getNumTreeSeries();
					if(numTreeSeries < 1)
						System.err.println("No tree data found");
					else if(choice == 10)
						determineTreePosterior(path, outFile, file1, datacontainer, burnin, formatter);
					else if(choice == 11)
						determineMaximumAPosterioriTree(path, outFile, datacontainer, burnin, file1, formatter);
					else {
						MCMCCommandLine.printLine(path, outFile, "{\n\t\"File\": \"" + file1 + "\",");
						MCMCCommandLine.printLine(path, outFile, "\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
						MCMCCommandLine.printLine(path, outFile, "\t\"Burnin\": " + burnin + ",");
						MCMCCommandLine.parallelConvergenceTest(path, alpha, datacontainer, datacontainer, burnin, (burnin + datacontainer.getValueSerie(0).size())/2, outFile);
					}
				}
			}
		} else if(file != null && !file2.equals("")) {
			parallelchainanalysis(burnin1, burnin2, path, outFile, alpha, formatter, file, file2);
		}
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public static void parallelConvergenceTest(String path, Double alpha, MCMCDataContainer datacontainer1, MCMCDataContainer datacontainer2, int burnin1, int burnin2, String outFile) throws Exception {
		ArrayList<String> series1 = new ArrayList<String>();
		ArrayList<String> series2 = new ArrayList<String>();

		for(int i = burnin1; i < datacontainer1.getTreeSerie(datacontainer1.getSelectedTreeIndex()).size(); i++)
			series1.add(new String(datacontainer1.getTreeSerie(datacontainer1.getSelectedTreeIndex()).get(i).getData()));
		for(int i = burnin2; i < datacontainer2.getTreeSerie(datacontainer2.getSelectedTreeIndex()).size(); i++)
			series2.add(new String(datacontainer2.getTreeSerie(datacontainer2.getSelectedTreeIndex()).get(i).getData()));

		ArrayList<String> res = MCMCTreeParamTesting.performTest(series1, series2, 0, 0, alpha, true);		
		if(Double.parseDouble(res.get(1)) < alpha)
			MCMCCommandLine.printLine(path, outFile, "\t\"Test statistic\" : " + res.get(0) + ",\n\t\"pValue\" : " + res.get(1) + ",\n\t\"Decision\" : Different distribution\n}");
		else
			MCMCCommandLine.printLine(path, outFile, "\t\"Test statistic\" : " + res.get(0) + ",\n\t\"pValue\" : " + res.get(1) + ",\n\t\"Decision\" : Same distribution\n}");
	}
	
	public static void printString(String path, String outFile, String str) throws Exception {
		if(outFile.equals("stdout")) 
			System.out.print(str);
		else
			SequenceFileWriter.writeAndAppendString(path, outFile, str);
	}
	
	private static int calculateBurnin(int burnin, MCMCDataContainer datacontainer) {
		int 						numSeries;
		int 						serieLength;
		int 						ess;
		int 						overallConvergenceESS;
		Object[] 					serie;
		
		overallConvergenceESS = -2;
		
		if(burnin == -1 && datacontainer != null) {
			numSeries = datacontainer.getNumValueSeries();
			if(numSeries != 0) {
				for (List<Double> i : datacontainer.getValueSeries()) {
					serie = i.toArray();
					serieLength	= serie.length;

					if(serieLength < 100){
						overallConvergenceESS = -1;
						break;
					}

					ess = MCMCMath.calculateESSmax(serie);

					if(overallConvergenceESS != -1 && ess > overallConvergenceESS) 
						overallConvergenceESS = ess;
				}
			} 
			if(overallConvergenceESS == -1) 
				burnin = 0;
			else
				burnin = overallConvergenceESS;
		}
		return burnin;
	}
	
	public static void printLine(String path, String outFile, String str) throws Exception {
		if(outFile.equals("stdout")) 
			System.out.println(str);
		else
			SequenceFileWriter.writeAndAppendLine(path, outFile, str);
	}
	
	public static void ComputeAndPrintStatistics(int burnin, Object[] serie, String path, String outFile, double confidencelevel) throws Exception {
		Double[] data;
		
		int 						numValues;
		int 						equalStart; 
		int 						equalEnd;
		int 						tempHolder;
		int 						intervalLength;
		int 						startPos; 
		int 						leftIndex;
		int 						rightIndex;
		double 						values;
		double 						values1;
		double 						mean;
		double 						sigmaSquare;
		double					 	arithmetic_standard_dev;
		double 						power;
		double 						geometric_mean;
		double 						sum;
		double 						sum1;
		double 						geometric_standard_dev;
		double 						harmonic_mean;
		double		 				max_value;
		double				 		min_value;
		double 						comp;
		double 						nearest;
		double				 		startNum;
		double 						tempHolder1;
		double 						tempHolder2;

		data = new Double[serie.length-burnin];
		System.arraycopy(serie, burnin, data, 0, serie.length-burnin);

		numValues = data.length;
		values = 0;
		values1	= 1;
		sigmaSquare = 0;
		power = (double) 1/numValues; 
		sum = 0;
		sum1 = 0;
		max_value = data[0];
		min_value = data[0];

		for(Double k : data) {
			values += k;
			values1	*= java.lang.Math.pow(k, power);
			sum += Math.pow(Math.log(k), 2);
			sum1 += (double)1/k;

			if(k > max_value)
				max_value = k;

			if(k < min_value)
				min_value = k;
		}
		mean = values/numValues;
		geometric_mean = values1;
		harmonic_mean = (double)numValues/sum1;

		for(Double k : data)
			sigmaSquare += java.lang.Math.pow(k - mean,2);

		arithmetic_standard_dev = (double)java.lang.Math.sqrt(sigmaSquare/(numValues-1));
		geometric_standard_dev = Math.abs(Math.exp(Math.sqrt(sum/(numValues-1) - ((numValues/(numValues-1))*Math.pow(Math.log(values1),2)))));

		MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Arithmetic Mean\": " + mean + ",");
		MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Arithmetic Standard Deviation\": " + arithmetic_standard_dev + ",");
		if(!Double.isNaN(geometric_mean))
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geometric Mean\": " + geometric_mean + ",");
		else
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geometric Mean\": \"" + geometric_mean + "\",");
		if(!Double.isNaN(geometric_standard_dev))
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geometric Standard Deviation\": " + geometric_standard_dev + ",");
		else
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Geometric Standard Deviation\": \"" + geometric_standard_dev + "\",");
		MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Harmonic Mean\": " + harmonic_mean + ",");
		MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Minimum Value\": " + min_value + ",");
		MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Maximum Value\": " + max_value + ",");

		Arrays.sort(data);
		nearest	=(Double)data[0];
		equalStart = 0;
		equalEnd = 0;

		for(int k = 0; k < numValues-1; k++) {
			comp = Math.abs(mean-nearest) - Math.abs(mean-(Double)data[k+1]);
			if (comp > 0) {
				nearest = (Double)data[k+1]; 
				equalStart = k+1;
			} else if (comp == 0) 
				equalEnd = k+1;
		}

		if(equalEnd == 0)
			tempHolder = equalStart;
		else
			tempHolder = equalStart + (equalEnd - equalStart)/2;

		double[] start = {nearest, tempHolder};
		intervalLength = (int) ((double)(numValues)*(confidencelevel/100));
		startPos = (int)start[1]; 
		startNum = start[0];
		leftIndex = startPos-1;
		rightIndex = startPos+1;

		MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Confidence_Level\": " + confidencelevel + ",");

		if(numValues == 0) {
			tempHolder1 = Double.NaN;
			tempHolder2 = Double.NaN;
			double[] result = {tempHolder1, tempHolder2};
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Credible Interval\": \"" + result[0] + " ; " + result[1] + "\"");
		} else if(numValues == 1) {
			tempHolder1 = (Double) data[0];
			tempHolder2 = (Double) data[0];
			double[] result = {tempHolder1, tempHolder2};
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Credible Interval\": \"" + result[0] + " ; " + result[1] + "\"");
		} else if(numValues == 2) {
			tempHolder1 = (Double) data[0];
			tempHolder2 = (Double) data[1];
			double[] result = {tempHolder1, tempHolder2};
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Credible Interval\":  \"" + result[0] + " ; " + result[1] + "\"");
		} else {
			for(int k = 0 ; k < intervalLength ; k++) {
				if(leftIndex == 0)
					if(rightIndex < numValues-1)
						rightIndex++;
				if(rightIndex == numValues-1)
					if(leftIndex > 0)
						leftIndex--;
				if(leftIndex > 0 && Math.abs((Double)data[leftIndex] - startNum) <= Math.abs((Double)data[rightIndex] - startNum))
					leftIndex--;
				else if(rightIndex < numValues-1 && Math.abs((Double)data[leftIndex] - startNum) > Math.abs((Double)data[rightIndex] - startNum))
					rightIndex++;
			}
			double[] result = {(Double) data[leftIndex], (Double) data[rightIndex]};
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Credible Interval\": \"" + result[0] + " ; " + result[1] + "\"");
		}
	}
	
	private static void parallelchainanalysis(int burnin1, int burnin2, String path, String outFile, double alpha, NumberFormat formatter, File file, String file2) throws Exception {
		File file3 = new File(path + file2);
		
		MCMCDataContainer datacontainer1 = MCMCFileReader.readMCMCFile(file, false, false);
		MCMCDataContainer datacontainer2 = MCMCFileReader.readMCMCFile(file3, false, false);
				
		burnin1 = calculateBurnin(burnin1, datacontainer1);
		burnin2 = calculateBurnin(burnin2, datacontainer2);
					
		MCMCCommandLine.printLine(path, outFile, "{\n\t\"File1\": \"" + file.getAbsolutePath() + "\",");
		MCMCCommandLine.printLine(path, outFile, "\t\"# Samples in file1\": \"" + datacontainer1.getNumLines() + "\",");
		MCMCCommandLine.printLine(path, outFile, "\t\"Burnin for file1\": \"" + burnin1 + "\",");
		MCMCCommandLine.printLine(path, outFile, "\t\"File2\": \"" + file3.getAbsolutePath() + "\",");
		MCMCCommandLine.printLine(path, outFile, "\t\"# Samples in file2\": \"" + datacontainer2.getNumLines() + "\",");
		MCMCCommandLine.printLine(path, outFile, "\t\"Burnin for file2\": \"" + burnin2 + "\",");
		MCMCCommandLine.printLine(path, outFile, "\t\"Alpha\": \"" + alpha + "\",");
		
		MCMCCommandLine.printLine(path, outFile, "\t\"Parameters\": {");
		
		for(int i = 0; i < datacontainer1.getValueSeries().size(); i++) {
			MCMCCommandLine.printLine(path, outFile, "\t\t\"" + datacontainer1.getValueNames().get(i) + "\": {");
			int numPoints1 			= datacontainer1.getValueSerie(i).size();	
			int numBurnInPoints1 	= burnin1;	
			int numPoints2 			= datacontainer2.getValueSerie(i).size();
			int numBurnInPoints2 	= burnin2;
			
			final double[] sample1 	= new double[numPoints1 - numBurnInPoints1];
			final double[] sample2 	= new double[numPoints2 - numBurnInPoints2];
			List<Double> temp1 = datacontainer1.getValueSerie(i);
			List<Double> temp2 = datacontainer2.getValueSerie(i);
			
			for(int j = 0 ; j < temp1.size() - numBurnInPoints1; j++) 
				sample1[j] = temp1.get(j + numBurnInPoints1);
			for(int j=0 ; j < temp2.size() - numBurnInPoints2; j++) 
				sample2[j] = temp2.get(j + numBurnInPoints2);

			MannWhitneyUTest mw  = new MannWhitneyUTest();
			double testStatistic = mw.mannWhitneyU(sample1,sample2);
			double pValue  = mw.mannWhitneyUTest(sample1,sample2);

			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Test statistic\" : " + testStatistic + ",");
			MCMCCommandLine.printLine(path, outFile, "\t\t\t\"p-value\" : " + formatter.format(pValue) + ",");
			if(pValue > alpha )
				MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Decision\" : \"Same distr.\"\n\t\t},");
			else
				MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Decision\" : \"Different distr.\"\n\t\t},");
		
			double min1 = sample1[0];
			double max1 = sample1[0];
			for(int j = 1; j < sample1.length; j++) {
				if(sample1[j] < min1)
					min1 = sample1[j];
				if(sample1[j] > max1)
					max1 = sample1[j];
			}
			
			for(int j = 0; j < sample2.length; j++) {
				if(sample2[j] < min1)
					min1 = sample2[j];
				if(sample2[j] > max1)
					max1 = sample2[j];
			}
			
			System.out.println("MinMax : " + min1 + "\t" + max1);
			
			int numberofbins = (sample2.length / 100) + 1;
			
			double[] binValues = new double[numberofbins];
			for(int j = 0; j < numberofbins; j++) {
				binValues[j] = min1 + (j * ((max1-min1)/(numberofbins-1)));
			}
			
			long[] s1 = new long[numberofbins];
			long[] s2 = new long[numberofbins];
			
			for(int j = 0; j < numberofbins; j++) {
				s1[j] = 0;
				s2[j] = 0;
			}
			
			for(int j = 0; j < sample1.length; j++) {
				for (int k = 1; k < numberofbins; k++) {
					if(sample1[j] >= binValues[k-1] && sample1[j] < binValues[k]) {
						s1[k] = s1[k] + 1;
						break;
					}
				}
			}
			
			for(int j = 0; j < sample2.length; j++) {
				for (int k = 1; k < numberofbins; k++) {
					if(sample2[j] >= binValues[k-1] && sample2[j] < binValues[k]) {
						s2[k] = s2[k] + 1;
						break;
					}
				}
			}
			
			long sum1 = 0;
			long sum2 = 0;
			for(int j = 0; j < binValues.length; j++) {
//				System.out.println(binValues[j] + "\t" + s1[j] + "\t" + s2[j]);
				sum1 = sum1 + s1[j];
				sum2 = sum2 + s2[j];
			}
			
			s1[numberofbins - 1] = s1[numberofbins - 1] + sample1.length - sum1;
			s2[numberofbins - 1] = s2[numberofbins - 1] + sample2.length - sum2;
			
			ArrayList<Long> list1 = new ArrayList<Long>();
			ArrayList<Long> list2 = new ArrayList<Long>();
			
			for(int j = 0; j < numberofbins; j++) {
				if(s1[j] > 0 && s2[j] > 0 && s1[j] > 50 && s2[j] > 50 ) {
					list1.add(s1[j]);
					list2.add(s2[j]);
				}
			}
			
			sum1 = 0;
			sum2 = 0;
			
			for(int j = 0; j < binValues.length; j++) {
				System.out.println(binValues[j] + "\t" + s1[j] + "\t" + s2[j]);
				sum1 = sum1 + s1[j];
				sum2 = sum2 + s2[j];
			}
			
			System.out.println("Net sum : " + sum1 + "\t" + sum2);
			
			sum1 = 0;
			sum2 = 0;
			
/*			for(int j = 0; j < list1.size(); j++) {
				System.out.println(list1.get(j) + "\t" + list2.get(j));
				sum1 = sum1 + list1.get(j);
				sum2 = sum2 + list2.get(j);
			}*/
			
			assert(list1.size() == list2.size());
			
			long [] data1 = new long[list1.size()];
			long [] data2 = new long[list2.size()];
					
			for(int j = 0; j < list1.size() ; j++ ) {
				data1[j] = list1.get(j).longValue();
				data2[j] = list2.get(j).longValue();
			}
			
			for(int j = 0; j < data1.length; j++) {
				System.out.println("Data1 Entry " + j + "\t\t" + data1[j] + "\t" + data2[j]);
				sum1 = sum1 + data1[j];
				sum2 = sum2 + data2[j];
			}
			
			assert(data1.length == data2.length);
			
			System.out.println("data1 Length = " + data1.length + " Data2 Length = " + data2.length);
			
			if(data1.length > 1) {
				ChiSquareTest chi  = new ChiSquareTest();

				ArrayList<String> res = new ArrayList<String>();
				res.add(String.valueOf(chi.chiSquareDataSetsComparison(data1,data2)));
				res.add(String.valueOf(chi.chiSquareTestDataSetsComparison(data1,data2)));

				System.out.println("Net sum : " + sum1 + "\t" + sum2);

				System.out.println("------------------------------------");

				if(Double.parseDouble(res.get(1)) < alpha)
					MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Test statistic\" : " + res.get(0) + ",\n\t\t\t\"pValue\" : " + res.get(1) + ",\n\t\t\t\"Decision\" : Different distribution\n\t\t},");
				else
					MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Test statistic\" : " + res.get(0) + ",\n\t\t\t\"pValue\" : " + res.get(1) + ",\n\t\t\t\"Decision\" : Same distribution\n\t\t},");
			} else if (data1.length == 1) {
				System.out.println("Net sum : " + sum1 + "\t" + sum2);

				System.out.println("------------------------------------");
				MCMCCommandLine.printLine(path, outFile, "\t\t\t\"Test statistic\" : " + 0 + ",\n\t\t\t\"pValue\" : " + 1 + ",\n\t\t\t\"Decision\" : Same distribution\n\t\t},");
			}
		}
		
		MCMCCommandLine.printLine(path, outFile, "\t\t\"Tree topology" + "\": {");
		
		MCMCCommandLine.parallelConvergenceTest(path, alpha, datacontainer1, datacontainer2, burnin1, burnin2, outFile);
	}
	
	private static void determineTreePosterior(String path, String outFile, String file1, MCMCDataContainer datacontainer, int burnin, NumberFormat formatter) throws Exception {
		int 						numBurnInPoints;
		int 						numPoints;
		int 						key;
		int 						numDuplicates;
		TreeMap<Integer, MCMCTree> 	treeMap;
		ArrayList<MCMCTree> 		uniqueserie;
		MCMCTree 					uniqueTree;
		ArrayList<TreeMap<Integer, MCMCTree>> 	treeMapList;
		ArrayList<MCMCTree> 		treeSerie;
		MCMCTree[] 					treeData;
		treeMapList	= new ArrayList<TreeMap<Integer, MCMCTree>>();

		int pos = 0;
		for(ArrayList<MCMCTree> i : datacontainer.getTreeSeries()) {
			treeMap = new TreeMap<Integer, MCMCTree>();
			treeMapList.add(treeMap);

			treeSerie 				= i;
			numPoints 				= treeSerie.size();	
			numBurnInPoints 		= burnin;
			treeData 				= new MCMCTree[numPoints-numBurnInPoints];

			System.arraycopy(treeSerie.toArray(), numBurnInPoints, treeData, 0, numPoints-numBurnInPoints);

			for(MCMCTree tree : treeData) {
				key = tree.getKey();
				uniqueTree = treeMap.get(key);

				if(uniqueTree == null) {
					uniqueTree = new MCMCTree();

					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
					uniqueTree.setData(tree.getData());
					treeMap.put(key, uniqueTree);
				} else {
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
				}
			}
			treeMap = treeMapList.get(pos);
			uniqueserie	= new java.util.ArrayList<MCMCTree>(treeMap.values());

			Collections.sort(uniqueserie);

			if(pos == 0) {
				MCMCCommandLine.printLine(path, outFile, "{\n\t\"File\": \"" + file1 + "\",");
				MCMCCommandLine.printLine(path, outFile, "\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
				MCMCCommandLine.printLine(path, outFile, "\t\"Burnin\": " + burnin + ",");
				MCMCCommandLine.printLine(path, outFile, "\t\"Trees\": {");
			}
			MCMCCommandLine.printLine(path, outFile, "\t\t\"Series_" + pos + "\": [");
			
			int pos1 = 0;
			for(MCMCTree j : uniqueserie) {
				MCMCCommandLine.printLine(path, outFile, "\t\t\t{");
				String newick = new String(j.getData());
				numDuplicates = j.getNumDuplicates();
				MCMCCommandLine.printString(path, outFile, "\t\t\t\t\"Index\": " + pos1 + ",\n\t\t\t\t\"Duplicates\": " + numDuplicates + ",\n\t\t\t\t\"Posterior probability\": " + (formatter.format((double) numDuplicates/(numPoints-numBurnInPoints))) + ",\n\t\t\t\t\"Newick\": \"" + newick + "\"" + "\n\t\t\t}");
				if(pos1 < uniqueserie.size()-1)
					MCMCCommandLine.printLine(path, outFile, ",");
				pos1++;
			}
			MCMCCommandLine.printString(path, outFile, "\n\t\t]");
			if(pos == 0 && datacontainer.getNumTreeSeries() > 1) 
				MCMCCommandLine.printLine(path, outFile, ",");
			else 
				MCMCCommandLine.printLine(path, outFile, "");
			pos++;
		}
	}
	
	private static void determineMaximumAPosterioriTree(String path, String outFile, MCMCDataContainer datacontainer, int burnin, String file1, NumberFormat formatter) throws Exception {
		int 						numPoints;	
		int 						numBurnInPoints;
		int 						key;
		TreeMap<Integer, MCMCTree> 	treeMap;
		ArrayList<MCMCTree> 		uniqueserie;
		MCMCTree 					uniqueTree;
		ArrayList<TreeMap<Integer, MCMCTree>> 	treeMapList;
		ArrayList<MCMCTree> 		treeSerie;
		MCMCTree[] 					treeData;
		
		treeMapList	= new ArrayList<TreeMap<Integer, MCMCTree>>();
		for(int i = 0; i < datacontainer.getNumTreeSeries(); i++) {
			treeMap = new TreeMap<Integer, MCMCTree>();
			treeMapList.add(treeMap);

			treeSerie = datacontainer.getTreeSerie(i);
			numPoints = treeSerie.size();	
			numBurnInPoints	= burnin;
			treeData = new MCMCTree[numPoints-numBurnInPoints];

			System.arraycopy(treeSerie.toArray(), numBurnInPoints, treeData, 0, numPoints-numBurnInPoints);

			for(MCMCTree tree : treeData) {
				key = tree.getKey();
				uniqueTree = treeMap.get(key);

				if(uniqueTree == null) {
					uniqueTree = new MCMCTree();

					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
					uniqueTree.setData(tree.getData());
					treeMap.put(key, uniqueTree);
				} else {
					uniqueTree.addIndex(tree.getIndexList().get(0));
					uniqueTree.addDuplicate();
				}
			}
			treeMap = treeMapList.get(i);
			uniqueserie	= new java.util.ArrayList<MCMCTree>(treeMap.values());

			Collections.sort(uniqueserie);

			uniqueTree = uniqueserie.get(0);

			String newickseq = new String(uniqueTree.getData());
			Integer duplicates = uniqueTree.getNumDuplicates();
			for(MCMCTree j : uniqueserie) {
				if(j.getNumDuplicates() > duplicates) {
					newickseq = new String(j.getData());
					duplicates = j.getNumDuplicates();
				}
			}
			if (i == 0) {
				MCMCCommandLine.printLine(path, outFile, "{\n\t\"File\": \"" + file1 + "\",");
				MCMCCommandLine.printLine(path, outFile, "\t\"Total_iterations\": " + datacontainer.getValueSerie(0).size() + ",");
				MCMCCommandLine.printLine(path, outFile, "\t\"Burnin\": " + burnin + ",");
				MCMCCommandLine.printLine(path, outFile, "\t\"Series\" : [");
			}
			
			MCMCCommandLine.printLine(path, outFile,"\t\t{");
			MCMCCommandLine.printLine(path, outFile,"\t\t\t\"Index\": " + i + ",");
			MCMCCommandLine.printLine(path, outFile,"\t\t\t\"MAP_Probability\": " + (formatter.format((double) duplicates/(numPoints-numBurnInPoints))) + ",");
			MCMCCommandLine.printLine(path, outFile,"\t\t\t\"MAP_Tree\": \"" + newickseq + "\"");
			if(i == 0 && datacontainer.getNumTreeSeries() > 1) 
				MCMCCommandLine.printLine(path, outFile,"\t\t},");
			else 
				MCMCCommandLine.printLine(path, outFile,"\t\t}");
		}
		MCMCCommandLine.printLine(path, outFile,"\t]");
	}
}
