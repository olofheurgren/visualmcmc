package se.cbb.vmcmc.libs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * 
 * @author Raja Hashim Ali
 *
 */
abstract public class SequenceFileWriter {
	/*'******************************************************************************
	 * 							CLASS PUBLIC FUNCTIONS								*
	 ********************************************************************************/
	public static void writeAndAppendString(String path, String fileName, String string) throws Exception {
		path 				= path.concat(fileName);
		FileWriter fstream 	= new FileWriter(path, true);
		BufferedWriter out 	= new BufferedWriter(fstream);

		try {
			out.write(string);
		} finally {
			try {
				out.flush();
				out.close();
			} catch (Exception e) {
				System.out.println("writeSequenceInFasta Failed: "+ e.getMessage());
				System.exit(-1);
			}
		} 
	}

	public static void writeAndAppendLine(String path, String fileName, String string) throws Exception {
		path 				= path.concat(fileName);
		FileWriter fstream 	= new FileWriter(path, true);
		BufferedWriter out 	= new BufferedWriter(fstream);

		try{
			out.write(string + "\n");
		} finally {
			try {
				out.flush();
				out.close();
			} catch (Exception e) {
				System.out.println("writeSequenceInFasta Failed: "+ e.getMessage());
				System.exit(-1);
			}
		} 
	}
	
	public static void createAndWriteString(String fileName, String string) throws Exception {
		BufferedWriter out 	= new BufferedWriter(new FileWriter("./" + fileName));

		out.write(string);
		
		out.flush();
		out.close();
	}
		
	public static boolean FileDeleter(String fileName){
		File in 		= new File("./" + fileName);
	    boolean success = in.delete();

	    if (!success){
	    	System.out.println("Deletion failed.");
	    }
		return success;
	}

	/*'******************************************************************************
	 * 							END OF CLASS										*
	 ********************************************************************************/
}
