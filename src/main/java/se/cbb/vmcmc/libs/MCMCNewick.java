package se.cbb.vmcmc.libs;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * MCMCNewick: Functions for converting MCMCTreeNodes to newick strings
 */
abstract public class MCMCNewick {

	private static String newick = "";

	public static void TreeToNewick(MCMCTreeNode node, int numTrees) {
		final NumberFormat 		formatter 	= new DecimalFormat("0.00");
		if(node.getNumChildren() > 0) 
			newick += "(";
		else
			newick += node.getName();
		for(int i = 0; i < node.numChildren; i++) {
			TreeToNewick(node.getChild(i), numTrees);
			if(i < node.getNumChildren()-1)
				newick += ", ";
			else if((double) node.getNumDuplicates()/numTrees <= 1)
				newick += ")" + String.valueOf(formatter.format(((double) node.getNumDuplicates()/numTrees)*100));
			else
				newick += ")";
		}
	}
	
	public static String getNewick(MCMCTreeNode node, int numTrees) {
		newick = "";
		TreeToNewick(node,numTrees);
		return newick+";";
	}
}