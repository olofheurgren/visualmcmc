package se.cbb.vmcmc.libs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.Box;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import se.cbb.vmcmc.gui.MCMCMainTab;
import se.cbb.vmcmc.gui.MCMCParallelMainTab;
import se.cbb.vmcmc.gui.MCMCTreeAnalysisTab;
import se.cbb.vmcmc.gui.MCMCTreeParallelTab;
import se.cbb.vmcmc.gui.MCMCTreeTab;
import se.cbb.vmcmc.gui.MCMCWindow;

public class MCMCTreeFunctions {
	/** Definition: 			Creates and updates new instance of MCMCMTreeTab and adds default components.
	<p>Usage: 				Used when tree data is found in the input file.						
 	<p>Function:			Build up teh Tree Panel using all the data from the file provided. 				
 	<p>Classes:				MCMCTreeTab, MCMCDataContainer. 												
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static MCMCTreeTab createTreePanel(MCMCDataContainer datacontainer, MCMCWindow window) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final MCMCTreeTab treePanel = new MCMCTreeTab(window);
		JPanel 						droplistPanel;
		int							burnin;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		droplistPanel 				= new JPanel();
		burnin 						= datacontainer.getBurnin();
		
		/* ******************** FUNCTION BODY ************************************* */
		treePanel.setDataContainer(datacontainer);
		treePanel.setSeriesID(0);
		double burninpercent = (double)burnin/(double)datacontainer.getNumLines();
		treePanel.setBurnIn(burninpercent);

		treePanel.getDropList().setModel(new DefaultComboBoxModel(datacontainer.getTreeNames().toArray()));
		
		droplistPanel.setOpaque(false);
		droplistPanel.add(treePanel.getDropList());

		treePanel.addToSouth(droplistPanel);
		treePanel.addToSouth(Box.createRigidArea(new Dimension(10, 0)));
		treePanel.addToSouth(Box.createRigidArea(new Dimension(10, 0)));

		return treePanel;
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static MCMCTreeParallelTab createTreeParallelPanel(MCMCDataContainer datacontainer, final MCMCWindow window) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final MCMCTreeParallelTab treePanel = new MCMCTreeParallelTab(window);
		JPanel 						droplistPanel;
		int							burnin;
		JComboBox 				alphadroplist;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		droplistPanel 				= new JPanel();
		burnin 						= datacontainer.getBurnin();
		String[] strArr = {"1% Significance Level", "5% Significance Level", "10% Significance Level"};
		
		/* ******************** FUNCTION BODY ************************************* */
		treePanel.setDataContainer(datacontainer);
		treePanel.setSeriesID(0);
		double burninpercent = (double)burnin/(double)datacontainer.getNumLines();
		treePanel.setBurnIn(burninpercent);
		
		alphadroplist = treePanel.getAlphaDropList();
		
		alphadroplist.setModel(new DefaultComboBoxModel(strArr)); 
		alphadroplist.setSelectedIndex(1);

		treePanel.getDropList().setModel(new DefaultComboBoxModel(datacontainer.getTreeNames().toArray()));
		
		droplistPanel.setOpaque(false);
		droplistPanel.add(treePanel.getDropList());

		treePanel.addToSouth(droplistPanel);
		treePanel.addToSouth(Box.createRigidArea(new Dimension(10, 0)));
		treePanel.addToSouth(Box.createRigidArea(new Dimension(10, 0)));

		treePanel.getAlphaDropList().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(treePanel.getAlphaDropList().getSelectedIndex() == 0)
					treePanel.setAlpha(0.01);
				else if(treePanel.getAlphaDropList().getSelectedIndex() == 1)
					treePanel.setAlpha(0.05);
				else
					treePanel.setAlpha(0.1);
				treePanel.updateChiSquarePanel(window);
			}
		});
		
		return treePanel;
		
		/* ******************** END OF FUNCTION *********************************** */
	}

	/** Definition: 			Creates and updates new instance of MCMCMTreeTab and adds default components.
	<p>Usage: 				Used when tree data is found in the input file.						
 	<p>Function:			Build up teh Tree Panel using all the data from the file provided. 				
 	<p>Classes:				MCMCTreeTab, MCMCDataContainer. 												
	 */
	public static MCMCTreeAnalysisTab createTreeAnalysisPanel(MCMCDataContainer datacontainer, int seriesID) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		final MCMCTreeAnalysisTab treeAnalysisPanel = new MCMCTreeAnalysisTab();
		int burnin;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		burnin = datacontainer.getBurnin();
		
		/* ******************** FUNCTION BODY ************************************* */
		treeAnalysisPanel.setDataContainer(datacontainer);
		double burninpercent = (double)burnin/(double)datacontainer.getNumLines();
		treeAnalysisPanel.setBurnIn(burninpercent);

		treeAnalysisPanel.setSeriesID(seriesID);
		return treeAnalysisPanel;
		
		/* ******************** END OF FUNCTION *********************************** */
	}

	/** Definition: 			Adds functionalty between Tree tab and the Main Tab.										
	<p>Usage: 										
 	<p>Function:			 				
 	<p>Classes:				MCMCTreeTab, MCMCMainTab.
	 */
	public static void linkTreesToMain(final MCMCTreeTab treePanel, final MCMCMainTab mainPanel) {
		/* ******************** FUNCTION VARIABLES ******************************** */
		JButton 		markTreeButton;
		final JTable 	table 	= treePanel.getTable();
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		markTreeButton 			= new JButton("Mark selection in graph");
		
		/* ******************** FUNCTION BODY ************************************* */
		markTreeButton.setBackground(Color.WHITE);
		markTreeButton.setToolTipText("Highlight the parameter values during the MCMC run for the selected tree(s). Switch to graph panel to view highlighted values.");

		markTreeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] rowindices = table.getSelectedRows();

				mainPanel.getGraphTool().clearGraphMarks();
				int treeSerieID = treePanel.getSeriesID();

				ArrayList<MCMCTree> uniqueTreeList = new java.util.ArrayList<MCMCTree>(treePanel.getTreeMap(treeSerieID).values());
				Collections.sort(uniqueTreeList);

				for(int i=0; i<rowindices.length; i++) {
					ArrayList<Integer> list = uniqueTreeList.get(rowindices[i]).getIndexList();
					mainPanel.getGraphTool().addMarksToGraph(list);
				}
			}
		});
		treePanel.addToSouth(markTreeButton);
		treePanel.addToSouth(Box.createRigidArea(new Dimension(10, 0)));

		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/** Definition: 		Adds functionality between Main tab and the Trees tab.										
	<p>Usage: 				Make the main panel and tree panel uniform.						
 	<p>Function:			Handle and mantain the uniformity of the burnin selection between tree panel and main panel. 				
 	<p>Classes:				MCMCMainTab, MCMCTreeTab		
	 */
	public static void linkMainToTrees(final MCMCMainTab mainPanel, final MCMCTreeTab treePanel) {
		/* ******************** FUNCTION BODY ************************************* */
		mainPanel.getGraphTool().getSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider source = (JSlider) arg0.getSource();
				double burnin = (double) source.getValue()/source.getMaximum();
				treePanel.setBurnIn(burnin);
			}
		});

		mainPanel.getBurnInField().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int serieSize = mainPanel.getDataContainer().getValueSerie(mainPanel.getSeriesID()).size();
				int fieldValue = 0;

				try {
					fieldValue = Integer.valueOf(mainPanel.getBurnInField().getText());
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(new JFrame(), "Not a valid number. Only numbers allowed.");
				}

				double burnin = (double) fieldValue/serieSize;
				treePanel.setBurnIn(burnin);
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}

	/** Definition: 		Adds functionality between Main tab and the Trees tab.										
	<p>Usage: 				Make the main panel and tree panel uniform.						
 	<p>Function:			Handle and mantain the uniformity of the burnin selection between tree panel and main panel. 				
 	<p>Classes:				MCMCMainTab, MCMCTreeTab		
	 */
	public static void linkMainToTreeAnalysis(final MCMCMainTab mainPanel, final MCMCTreeAnalysisTab treePanel) {
		/* ******************** FUNCTION BODY ************************************* */
		mainPanel.getGraphTool().getSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider source = (JSlider) arg0.getSource();
				double burnin = (double) source.getValue()/source.getMaximum();

				treePanel.setBurnIn(burnin);
				treePanel.setredraw();
			}
		});

		mainPanel.getBurnInField().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int serieSize = mainPanel.getDataContainer().getValueSerie(mainPanel.getSeriesID()).size();
				int fieldValue = 0;

				try {
					fieldValue = Integer.valueOf(mainPanel.getBurnInField().getText());
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(new JFrame(), "Not a valid number. Only numbers allowed.");
				}

				double burnin = (double) fieldValue/serieSize;

				treePanel.setBurnIn(burnin);
				treePanel.setredraw();
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public static void linkMainToTreesParallel(final MCMCParallelMainTab mainPanel, final MCMCTreeParallelTab treePanel, final MCMCWindow parent) {
		/* ******************** FUNCTION BODY ************************************* */
		mainPanel.getGraphTool().getSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider source = (JSlider) arg0.getSource();
				
				double burnin = (double) source.getValue()/source.getMaximum();
				treePanel.setBurnIn(burnin);
			}
		});

		mainPanel.getBurnInField().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int serieSize = mainPanel.getDataContainer().getValueSerie(mainPanel.getSeriesID()).size();
				int fieldValue = 0;

				try {
					fieldValue = Integer.valueOf(mainPanel.getBurnInField().getText());
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(new JFrame(), "Not a valid number. Only numbers allowed.");
				}

				double burnin = (double) fieldValue/serieSize;
				treePanel.setBurnIn(burnin);
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}

	/** Definition: 		Adds functionality between Main tab and the Trees tab.										
	<p>Usage: 				Make the main panel and tree panel uniform.						
 	<p>Function:			Handle and mantain the uniformity of the burnin selection between tree panel and main panel. 				
 	<p>Classes:				MCMCMainTab, MCMCTreeTab		
	 */
	public static void linkMainToTreeAnalysisParallel(final MCMCParallelMainTab mainPanel, final MCMCTreeAnalysisTab treePanel) {
		/* ******************** FUNCTION BODY ************************************* */
		mainPanel.getGraphTool().getSlider().addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				JSlider source = (JSlider) arg0.getSource();
				double burnin = (double) source.getValue()/source.getMaximum();

				treePanel.setBurnIn(burnin);
				treePanel.setredraw();
			}
		});

		mainPanel.getBurnInField().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int serieSize = mainPanel.getDataContainer().getValueSerie(mainPanel.getSeriesID()).size();
				int fieldValue = 0;

				try {
					fieldValue = Integer.valueOf(mainPanel.getBurnInField().getText());
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(new JFrame(), "Not a valid number. Only numbers allowed.");
				}

				double burnin = (double) fieldValue/serieSize;

				treePanel.setBurnIn(burnin);
				treePanel.setredraw();
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/** Definition: 			Adds the applications default listeners for the tabs associated with current file.										
 	<p>Function:			 Update Tree Maps and tabel in the tree panel based on the Event Change.								
	 */
	public static void linkTabsToTrees(final JTabbedPane tabs, final MCMCTreeTab treePanel) {
		tabs.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if(tabs.getSelectedIndex() == 3) {
					treePanel.updateTreeMaps();
					treePanel.updateTable();
				}
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public static void linkTabsToTreesParallel(final JTabbedPane tabs, final MCMCTreeParallelTab treePanel, final MCMCWindow parent) {
		tabs.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				treePanel.updateTreeMaps();
				treePanel.updateTable();
				treePanel.updateChiSquarePanel(parent);
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	/** Definition: 			Adds the applications default listeners for the tabs associated with current file.										
 	<p>Function:			 Update Tree Maps and tabel in the tree panel based on the Event Change.								
	 */
	public static void linkTabsToTreeAnalysis(final JTabbedPane tabs, final MCMCTreeAnalysisTab treeAnalysisPanel) {
		tabs.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if(tabs.getSelectedIndex() == 4) {
					treeAnalysisPanel.updateTreeMaps();
					treeAnalysisPanel.updateTable();
				}
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}
	
	public static void linkTabsToTreeAnalysisParallel(final JTabbedPane tabs, final MCMCTreeAnalysisTab treeAnalysisPanel) {
		tabs.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if(tabs.getSelectedIndex() == 3) {
					treeAnalysisPanel.updateTreeMaps();
					treeAnalysisPanel.updateTable();
				}
			}
		});
		
		/* ******************** END OF FUNCTION *********************************** */
	}
}
