package se.cbb.vmcmc.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

import org.forester1.archaeopteryx.MainFrame;
import org.forester1.archaeopteryx.PdfExporter;

import se.cbb.vmcmc.gui.MCMCWindow;
import se.cbb.vmcmc.main.MCMCCommandLine;
import se.cbb.vmcmc.examples.MCMCExamples;
import se.cbb.vmcmc.main.MCMCAboutAndHelpItems;
import se.cbb.vmcmc.main.MCMCFileAnalyser;


/**																							
 * 	The main class for VMCMC. It is responsible for efficiently co-ordinating calls between various GUI classes, Data handling classes, User requirements and MCMC statistics computation and convergence test classes. 
 *	The main method is to get the filename and the type of results a user wants from this program and to use the function calls from various other implemented classes to generate the required graphics and/or results.
 * 
 *   <p>This file is part of the bachelor thesis "Verktyg för visualisering av MCMC-data" - VMCMC
 *   Royal Institute of Technology, Sweden. (M.Bark, J. Miró)
 *  <p>This file is part of PhD project work for Royal Institute of Technology. (R. H. Ali)
 *
 *	@author Mikael Bark, J. Miró and Raja Hashim Ali
 *	@param filename, burnin, confidence_level.
 *	@return Statistical and convergence analysis of MCMC output from CODA, JPRiME and PRiME.
 *	@Usage java MCMCApplication [-h] [-f FILENAME] [[-b burnin] [-c confidencelevel] [-n] [-s] [-t] [-e] [-r] [-g]]
 */
public class MCMCApplication extends MainFrame {
	private static final long serialVersionUID = 1L;
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private MCMCWindow window;
	private final JMenuItem parallelChain;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	/** Definition: 			Default constructor for VMCMC.										
	 *	<p>Usage: 				When no filename is provided as input parameter.						
 	 *	<p>Function:			Opens up the first window that is used to supply input file. 				
 	 *	<p>Classes: 			MCMCWindow.
 	 *	<p>Internal Functions:	createMenuBar(). 		
 	 *	@return: 				A new graphical basic window by invoking MCMCWindow.					
	 */
	public MCMCApplication() {
		/* ******************** FUNCTION VARIABLES ******************************** */
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		window = new MCMCWindow();
		parallelChain = new JMenuItem("Parallel Chain Analysis");
		
		/* ******************** FUNCTION BODY ************************************* */
		window.setTitle("VMCMC");	
		window.setJMenuBar(createMenuBar());	
		window.validate();

		UIManager.put("TabbedPane.selected"			, new Color(0xFFEEEEFF));	
		UIManager.put("TabbedPane.contentAreaColor"	, new Color(0xFFEEEEFF));
		UIManager.put("TabbedPane.shadow"			, new Color(0xFF000000));
		
		/* ******************** END OF FUNCTION *********************************** */				
	}


	/** Definition: 			Constructor with filename for VMCMC.										
	<p>Usage: 				When a filename is provided as input parameter.						
 	<p>Function:			Opens up the second window that displays all graphs and statistical analysis. 
 	<p>Classes: 		MCMCWindow.		
	<p>Internal Functions:	createDirectMenuBar(File). 		
 	@return: 				A new graphical basic window by invoking MCMCWindow that has all statistics and convergence tests for the MCMC file performed.					
	 */
	public MCMCApplication(String filename) {
		this(); // Call basic constructor first, then continue below and open
				// the tracefile

		File tracefile = new File(filename);

		if (tracefile != null) {
			parallelChain.setVisible(true);
			MCMCFileAnalyser.fileOpener(tracefile, window, false, false);
		}
	}

	/** Definition: 			Constructor with filename, custom burnin and/or confidence level for VMCMC.										
	<p>Usage: 				When filename and custom burnin/confidence level/both are provided as input parameters.						
 	<p>Function:			Opens up the second window that displays all graphs and statistical analysis.  				
 	<p>Classes: 			MCMCWindow, MCMCDataContainer, MCMCFileReader, MCMCMath.		
	<p>Internal Functions:	None. 				
 	@return: 				A new graphical basic window by invoking MCMCWindow.					
	 */
/*	public MCMCApplication(int choice, String file1, String file2, int burnin, double confidencelevel, String path, String outFile, Double alpha, int burnin1, int burnin2)  throws Exception {
		// Surely this is a redundant constructor?
		MCMCCommandLine.commandline(choice, file1, file2, burnin, confidencelevel, path, outFile, alpha, burnin1, burnin2);
	}
*/
	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	/** Definition: 			Private method for opening the first graphical window for VMCMC.										
	<p>Usage: 				When no filename is provided as input parameter, the default constructor with no file input refers to this function for coming up with the first interface.		
 	<p>Function:			Populates the first window and all its other options like about and exit etc. that are displayed in the first window. 	
 	<p>Classes: 			MCMCDataContainer, MCMCFileReader, MCMCMath.		
	<p>Internal Functions:	None.		
 	@return: 				A populated graphical basic window and handles all options used inside it.					
	 */
	private JMenuBar createMenuBar() {
		/* ******************** FUNCTION VARIABLES ******************************** */
		JMenuBar 		menubar;
		JMenu 			menuFile;
		JMenu 			VMCMCExamples;
		JMenuItem 		itemOpen;
		JMenuItem 		itemClose;
//		final JMenuItem parallelChain;
		JMenu 			menuAbout;  
		JMenuItem 		itemAbout;
		JMenuItem 		tutorial;
		JMenuItem 		PrimeExampleItem;
		JMenuItem		JprimeExampleItem;
		JMenuItem		MrbayesExampleItem;
		JMenuItem		LargeExampleItem;
		JMenuItem		MrbayesParallelExampleItem;
		JMenuItem		MrBayesStationaryTreeExampleItem;
		JMenuItem		BeastExampleItem;
		JMenuItem		SaveCurrentTab;
		JMenuItem		SampleParallelRunItem;
//		JMenuItem		SampleParallelConvergedItem;
		
		/* ******************** VARIABLE INITIALIZERS ***************************** */
		menubar =  new JMenuBar();
		menuFile = new JMenu("File");
		itemOpen = new JMenuItem("Open file");
//		parallelChain = new JMenuItem("Parallel Chain Analysis");
		itemClose = new JMenuItem("Exit");
		menuAbout = new JMenu("Help");  
		itemAbout = new JMenuItem("About Visual MCMC");
		tutorial = new JMenuItem("Help");
		VMCMCExamples = new JMenu("Examples");
		PrimeExampleItem = new JMenuItem("Sample Run - PrIMe");
		JprimeExampleItem = new JMenuItem("Sample Run - JPrIMe");
		LargeExampleItem = new JMenuItem("Sample Run - Uniform Tree Posterior");
		MrbayesParallelExampleItem = new JMenuItem("Sample Run (Parallel) - MrBayes");
		MrbayesExampleItem = new JMenuItem("Sample Run - MrBayes");
		MrBayesStationaryTreeExampleItem	= new JMenuItem("Sample Run - MrBayes Stationary Tree");
		BeastExampleItem	= new JMenuItem("Sample Run - Beast");
		SaveCurrentTab = new JMenuItem("Save Current View");
		SampleParallelRunItem = new JMenuItem("Sample Parallel Run - JPrIMe");
//		SampleParallelConvergedItem = new JMenuItem("Sample Parallel Run - PrIMe");
		
		/* ******************** FUNCTION BODY ************************************* */
		parallelChain.setVisible(false);
		
		//Menu actionlistener responsible for closing application.
		itemClose.addActionListener(new ActionListener() {   
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		SaveCurrentTab.addActionListener(new ActionListener() {   
			public void actionPerformed(ActionEvent arg0) {
				saveImageToFile(window);
			}
		});

		//Menu actionlistener responsible for creating filechooser, datacontainer and tabs.
		itemOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {   
				File file = null;

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;

						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension = name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("mcmc")||extension.equals("txt") || extension.equals("nex") || extension.equals("p")) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "MCMC Files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION) {
					file = chooser.getSelectedFile();
					parallelChain.setVisible(true);
					MCMCFileAnalyser.fileOpener(file, window, false, false);
				}
			}
		});
		
		parallelChain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {   
				File file1 = null;

				//Filter class responsible for filtering out all but mcmc files and folders
				class MCMCFilter extends FileFilter {
					public boolean accept(File file) {
						if(file.isDirectory())	//Show folders inside chooser
							return true;

						String extension = null;
						String name = file.getName();
						int i = name.lastIndexOf('.');

						if(i > 0 &&  i < name.length() - 1) 
							extension 		= name.substring(i+1).toLowerCase();	//Extract file extension

						if(extension != null) {
							if (extension.equals("mcmc")||extension.equals("txt") ) 	//Show files with extension mcmc
								return true;
							else 
								return false;
						}
						return false;
					}

					public String getDescription() {  //Description of filter
						return "MCMC Files";
					}
				}

				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new MCMCFilter());	//Add custom filter to file chooser

				int returnValue = chooser.showOpenDialog(chooser);
				if(returnValue == JFileChooser.APPROVE_OPTION)
					file1 = chooser.getSelectedFile();

				if(file1 != null) {					
					MCMCFileAnalyser.fileOpener(file1, window, true, true);
					MCMCFileAnalyser.parallelFileOpener(file1, window);
					parallelChain.setVisible(false);
				}
			}
		});

		//Menu actionlistener responsible for displaying "about" window
		itemAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MCMCAboutAndHelpItems.itemAboutMenu(window);
			}
		});
		
		PrimeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mcmcExample(window, "PrIMe");
			}
		});
		
		JprimeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mcmcExample(window, "JPrIMe");
			}
		});
		
		LargeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mcmcLargeDatasets(window);
			}
		});
		
		MrBayesStationaryTreeExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mcmcExample(window, "MrBayesStationary");
			}
		});
		
		BeastExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.beastExample(window);
			}
		});
		
		MrbayesParallelExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mrBayesParallelExample(window);
			}
		});
		
		MrbayesExampleItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mrBayesExample(window);
			}
		});
		
		SampleParallelRunItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mcmcParallelExample(window);
			}
		});
		
/*		SampleParallelConvergedItem.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCExamples.mcmcConvergedParallelExample(window);
			}
		});
*/		
		tutorial.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				MCMCAboutAndHelpItems.openTutorial(window);
			}
		});
		menuFile.add(itemOpen);
		menuFile.addSeparator();
		VMCMCExamples.add(PrimeExampleItem);
		VMCMCExamples.add(JprimeExampleItem);
		VMCMCExamples.add(BeastExampleItem);
		VMCMCExamples.add(MrbayesExampleItem);
		VMCMCExamples.add(MrBayesStationaryTreeExampleItem);
		VMCMCExamples.add(LargeExampleItem);
		VMCMCExamples.addSeparator();
		VMCMCExamples.add(MrbayesParallelExampleItem);
		VMCMCExamples.add(SampleParallelRunItem);
//		VMCMCExamples.add(SampleParallelConvergedItem);
		menuFile.add(SaveCurrentTab);
		menuFile.addSeparator();
		menuFile.add(parallelChain);
		menuFile.add(itemClose);
		menubar.add(menuFile);
		menubar.add(VMCMCExamples);
		menuAbout.add(tutorial);
		menuAbout.add(itemAbout);
		menubar.add(menuAbout);
		return menubar;
		
		/* ******************** END OF FUNCTION *********************************** */
	}

	/** Definition: 			Private method for opening the second graphical window directly for VMCMC.										
	<p>Usage: 				When a filename is provided as input parameter, the second constructor with filename refers to this function for coming up with the second interface.		
 	<p>Function:			Populates the second window with all its other options and all the statitical computations and convergence tests, that are displayed in the second window. 	
 	<p>Classes: 			MCMCDataContainer, MCMCFileReader, MCMCMath.		
	<p>Internal Functions:	None.		
 	@return: 				A populated graphical extended window that displays parameter values, sattistics, tests and trees found in the input file.					
	 */
	private JMenuBar createDirectMenuBar(File file) {
		JMenuBar menubar = createMenuBar();	// Create the first basic window
		if(file != null) {
			parallelChain.setVisible(true);
			MCMCFileAnalyser.fileOpener(file, window, false, false);
		}
		return menubar;
	}
	/* **************************************************************************** */

	private void saveImageToFile(MCMCWindow bill)  {
	    JFileChooser fc = new JFileChooser();
	    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    int returnVal = fc.showSaveDialog(MCMCApplication.this);
	    if (returnVal == JFileChooser.APPROVE_OPTION) {
	    	File file = fc.getSelectedFile();
	    	try {
	    		PdfExporter.writeToPdf( file.getAbsolutePath(), bill, bill.getWidth(), bill.getHeight() );
	    	}
	    	catch(Exception e)  {
	    		e.printStackTrace();
	    	}
	    }
	}
	
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */	
	/** Definition: 			Returns the private variable .											  		
 	@return 					Graphical window.				
	 */
	public MCMCWindow getWindow() {return window;}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
