package se.cbb.vmcmc.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import se.cbb.vmcmc.libs.MCMCMath;

import java.lang.Math;

/**
 * MCMCMainTab: Tab panel for the main tab. Central to all numerical analysis of a file.
 */
public class MCMCMainTab extends MCMCStandardTab {
	/* **************************************************************************** *
	 * 							CLASS VARIABLES										*
	 * **************************************************************************** */
	private static final long 	serialVersionUID = 1L;
	private static ArrayList<Integer> EssMaxList;
	private static ArrayList<Integer> gewekeMaxList;
	private MCMCGraphToolPanel 	graphtoolPanel;
	private MCMCDisplayPanel 	filePanel;
	private MCMCDisplayPanel 	mathPanel;
	private MCMCDisplayPanel 	burninPanel;
	@SuppressWarnings("rawtypes")
	private JComboBox 			droplist;
	private JTextField 			burninField;
	private Thread[] 			workerThreads;
	private Lock 				displaypanelLock;
	private double 				confidencelevel;
	
	/* **************************************************************************** *
	 * 							CLASS CONSTRUCTORS									*
	 * **************************************************************************** */
	public MCMCMainTab(MCMCWindow parent, ArrayList<Integer> essList, ArrayList<Integer> gewekeList) {
		super();

		displaypanelLock = new ReentrantLock();
		EssMaxList = essList;
		gewekeMaxList = gewekeList;
		confidencelevel = 0.95;

		graphtoolPanel = new MCMCGraphToolPanel();
		
		workerThreads = new Thread[9];

		for(int i=0; i<workerThreads.length; i++)
			workerThreads[i] = new Thread();

		westpanel.setLayout(new FlowLayout());
		westpanel.setMinimumSize(new Dimension(300, 0));
		westpanel.setPreferredSize(new Dimension(300, 0));
		westpanel.setBackground(new Color(0xFFEEEEFF));

		northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.X_AXIS));

		southPanel.setMinimumSize(new Dimension(0, 28));
		southPanel.setPreferredSize(new Dimension(0, 28));

		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));

		southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.X_AXIS));
		southPanel.setBackground(new Color(0xFFEEEEFF));

		centerPanel.add(graphtoolPanel);
		westpanel.add(createDisplayPanels());
		
		initDefaultButtons(parent);
	}
	
	/* **************************************************************************** *
	 * 							CLASS PRIVATE FUNCTIONS								*
	 * **************************************************************************** */
	/** initDefaultButtons: Add default buttons for the bottom panel.*/
	private void initDefaultButtons(final MCMCWindow parent) {
		JButton clearTreeButton = new JButton("Clear Tree Markings");
		clearTreeButton.setBackground(Color.WHITE);
		clearTreeButton.setToolTipText("Clear the tree markings on the graph selected previously from Tree panel.");
		
		//Button listener - removes tree markings inside the graph.
		clearTreeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graphtoolPanel.clearGraphMarks();
			}
		});
		
		southPanel.add(Box.createHorizontalGlue());
		southPanel.add(clearTreeButton);
		southPanel.add(Box.createRigidArea(new Dimension(10, 0)));
	}

	/** CreateDisplayPanels: Creates and adds panels responsible showing, file information, statistics and burn in. */
	@SuppressWarnings("rawtypes")
	private JPanel createDisplayPanels() {
		JPanel panel = new JPanel();

		final JTextField confidencelevelField = new JTextField();	//Text field for bayesian confidence level
		burninField = new JTextField();	//Text field for burn in

		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(new Color(0xFFEEEEFF));

		String[] paramnames = {"Parameter Selected:"};
		String[] burninNames = {"Burn in:", "Enter burnin", "Est. burnin (ESS): ", "Est. burnin (Geweke): ", "Gelman-Rubin Est. : "};

		JComponent[] burninComponents = {new JLabel(), burninField, new JLabel(), new JLabel(), new JLabel()};

		confidencelevelField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					confidencelevel = Double.parseDouble(confidencelevelField.getText())/100;
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(new JFrame(), "Not a valid number. Only numbers 0-100 allowed.");
				}

				if(confidencelevel > 1.0)
					confidencelevel = 1.0;
				else if(confidencelevel < 0)
					confidencelevel = 0;

				updateDisplayPanels();
			}
		});

		droplist = new JComboBox();
		droplist.setBackground(Color.WHITE);

		filePanel = new MCMCDisplayPanel(paramnames, "Parameter");
		mathPanel = new MCMCDisplayPanel("Parameter Statistics");

		mathPanel.addComponent("Arithmetic mean: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Arithmetic Std. Dev.: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Geometric mean: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Geometric Std Dev.: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Harmonic mean: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Maximum value: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Minimum value: ", new JLabel(), new Dimension(275, 18));
		mathPanel.add(Box.createRigidArea(new Dimension(0, 7)));
		mathPanel.addComponent("Confidence level: ", new JLabel(), new Dimension(275, 18));
		mathPanel.addComponent("Credible interval: ", new JLabel(), new Dimension(275, 18));

		burninPanel = new MCMCDisplayPanel("Burnin and Convergence");

		mathPanel.add(confidencelevelField);
		mathPanel.addComponent("Enter confidence level:", confidencelevelField, new Dimension(275, 18));

		filePanel.add(Box.createRigidArea(new Dimension(0, 5)));
		filePanel.add(droplist);
		filePanel.add(Box.createRigidArea(new Dimension(0, 5)));
		
		filePanel.setToolTipText("Parameter Panel");
		mathPanel.setToolTipText("Parameter Statistics Panel");
		burninPanel.setToolTipText("Burnin and Convergence Panel");

		burninPanel.addComponents(burninNames, burninComponents, new Dimension(275, 18));

		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(filePanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(mathPanel);
		panel.add(Box.createRigidArea(new Dimension(0, 2)));
		panel.add(burninPanel);

		return panel;
	}
		
	/* **************************************************************************** *
	 * 							CLASS PUBLIC FUNCTIONS								*
	 * **************************************************************************** */
	/** updateDisplayPanels: Will calculate, format and display information for each display panel in the left panel. */
	public void updateDisplayPanels(){
		final NumberFormat formatter = new DecimalFormat("0.#####E0");

		if(datacontainer != null) {
			final Object[] serie 		= datacontainer.getValueSerie(seriesID).toArray();
			String[] paramName 			= {datacontainer.getFileName()};

			filePanel.update(paramName);
			mathPanel.labels.get(7).setText(confidencelevel*100 + "%");

			int numPoints 				= serie.length;	
			final int numBurnInPoints 	= (int)(numPoints*burnin);		
			int ess;
			int geweke;
			boolean gelmanRubin;
			
			if(numPoints < 100) {
				geweke 					= -2;
				ess 					= -1;
				gelmanRubin				= false;
			} else {
				ess 					= EssMaxList.get(seriesID);
				geweke 					= gewekeMaxList.get(seriesID);
				gelmanRubin 			= MCMCMath.GelmanRubinTest(serie, numBurnInPoints);
			}

			if(numPoints-numBurnInPoints != 0) {
				burninPanel.getValueLabel(0).setText(String.valueOf(numBurnInPoints));
				if (ess != -1)
					burninPanel.getValueLabel(2).setText(String.valueOf(ess));
				else
					burninPanel.getValueLabel(2).setText(String.valueOf("Short Input"));
				
				if (geweke == -2)
					burninPanel.getValueLabel(3).setText(String.valueOf("Short Input"));
				else if(geweke == -1)
					burninPanel.getValueLabel(3).setText(String.valueOf("Not Converged"));
				else
					burninPanel.getValueLabel(3).setText(String.valueOf(geweke));
				
				burninPanel.getValueLabel(4).setText(String.valueOf(gelmanRubin));
				burninField.setText(String.valueOf(numBurnInPoints));				

				for(int t=0; t<workerThreads.length; t++)
					workerThreads[t].interrupt();
				
				final Double[] data 	= new Double[serie.length-numBurnInPoints];
				System.arraycopy(serie, numBurnInPoints, data, 0, serie.length-numBurnInPoints);

				//Thread updating arithmetic mean.
				workerThreads[0] = new Thread() {
					public void run() {
						double values 	= 0;

						for(Double i : data)
							values += i;

						double result 	= values/data.length;

						displaypanelLock.lock();
						mathPanel.labels.get(0).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[0].start();

				//Thread updating arithmetic standard deviation.
				workerThreads[1] = new Thread() {
					public void run() {
						
						double sigmaSquare = 0;
						double mean, values = 0;
						int i;
						for(i = 0; i < data.length; i++)
							values+= (Double)data[i];
						
						i = 0;
						mean = values/data.length;
						for(; i < data.length; i++)
							sigmaSquare+= java.lang.Math.pow((Double)data[i] - mean,2);

						double result = (double)java.lang.Math.sqrt(sigmaSquare/(i-1));

						displaypanelLock.lock();
						mathPanel.labels.get(1).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[1].start();

				//Thread updating geometric mean
				workerThreads[2] = new Thread() {
					public void run() {
						double values 	= 0;
						
						for(int i = 0; i < data.length; i++) 
							values += java.lang.Math.log((Double)data[i]);
						
						double result = java.lang.Math.exp(values/(double)data.length);
			
						displaypanelLock.lock();
						if(Double.isNaN(result))
							mathPanel.labels.get(2).setText("-");
						else
							mathPanel.labels.get(2).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[2].start();

				//Thread updating geometric standard deviation
				workerThreads[3] = new Thread() {
					public void run() {
				        double sum = 0;
				        double values 	= 0;
						
						for(Double i : data) 
							values += java.lang.Math.log(i);
						
						values = java.lang.Math.exp(values/(double)data.length);
						
				        int numValues = data.length;
				        
				        for(Double i : data) 
				            sum += Math.pow(Math.log(i), 2);
				        double result;
				        if(numValues <= 1)
				        	result = 0;
				        else
				        	result = Math.abs(Math.exp(Math.sqrt(sum/(numValues-1) - ((numValues/(numValues-1))*Math.pow(Math.log(values),2)))));
				        
						displaypanelLock.lock();
						if(Double.isNaN(result))
							mathPanel.labels.get(3).setText("-");
						else
							mathPanel.labels.get(3).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[3].start();

				//Thread updating harmonic mean
				workerThreads[4] = new Thread() {
					public void run() {
						double sum = 0;
						for(Double i : data)
							sum += (double)1/i;
						
						double result = (double)data.length/sum;
						displaypanelLock.lock();
						mathPanel.labels.get(4).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[4].start();

				//Thread updating maximum value
				workerThreads[5] = new Thread() {
					public void run() {
						double result = data[0];
						for(double value : data)
							if(value > result)
								result = value;
						
						displaypanelLock.lock();
						mathPanel.labels.get(5).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[5].start();

				//Thread updating minimum value
				workerThreads[6] = new Thread() {
					public void run() {
						double result = data[0];
						for(double value : data)
							if(value < result)
								result = value;
						
						displaypanelLock.lock();
						mathPanel.labels.get(6).setText(String.valueOf(formatter.format(result)));
						displaypanelLock.unlock();
					}
				};
				workerThreads[6].start();

				//Thread updating bayesian confidence interval
				workerThreads[7] = new Thread() {
					public void run() {
						Arrays.sort(data);
						
						double values 		= 0;

						for(Double i : data)
							values			+= i;
						
						double aritMean 	= values/data.length;
						double comp;
						double nearest		=(Double)data[0];
						int equalStart 		= 0;
						int equalEnd 		= 0;
						int tempHolder;

						for(int i = 0; i < data.length-1; i++) {
							comp = Math.abs(aritMean-nearest) - Math.abs(aritMean-(Double)data[i+1]);
							if(comp > 0) {
								nearest	 	= (Double)data[i+1]; 
								equalStart 	= i+1;
							}
							if(comp == 0) 
								equalEnd 	= i+1;
						}
						if(equalEnd == 0)
							tempHolder 		= equalStart;
						else
							tempHolder 		= equalStart + (equalEnd - equalStart)/2;

						double[] start 		= {nearest, tempHolder};
						int intervalLength 	= (int) ((double)(data.length)*(confidencelevel));
						int startPos 		= (int)start[1]; 
						int leftIndex 		= startPos-1;
						int rightIndex 		= startPos+1;
						double startNum 	= start[0];
						double tempHolder1;
						double tempHolder2;

						if(data.length == 0) {
							tempHolder1 	= Double.NaN;
							tempHolder2 	= Double.NaN;
							double[] result = {tempHolder1, tempHolder2};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(result[0] + " ; " + result[1]));
							displaypanelLock.unlock();
						} else if(data.length == 1) {
							tempHolder1 	= (Double) data[0];
							tempHolder2 	= (Double) data[0];
							double[] result = {tempHolder1, tempHolder2};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(result[0] + " ; " + result[1]));
							displaypanelLock.unlock();
						} else if(data.length == 2) {
							tempHolder1 	= (Double) data[0];
							tempHolder2 	= (Double) data[1];
							double[] result = {tempHolder1, tempHolder2};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(result[0] + " ; " + result[1]));
							displaypanelLock.unlock();
						} else {
							for(int i = 0 ; i < intervalLength ; i++) {
								if(leftIndex == 0)
									if(rightIndex < data.length-1)
										rightIndex++;

								if(rightIndex == data.length-1)
									if(leftIndex > 0)
										leftIndex--;

								if(leftIndex > 0 && Math.abs((Double)data[leftIndex] - startNum) <= Math.abs((Double)data[rightIndex] - startNum))
									leftIndex--;
								else if(rightIndex < data.length-1 && Math.abs((Double)data[leftIndex] - startNum) > Math.abs((Double)data[rightIndex] - startNum))
									rightIndex++;
							}
							double[] result = {(Double) data[leftIndex], (Double) data[rightIndex]};

							displaypanelLock.lock();
							mathPanel.labels.get(8).setText(String.valueOf(result[0] + " ; " + result[1]));
							displaypanelLock.unlock();
						}
					}
				};
				workerThreads[7].start();
			} else {
				//If burn in includes all data points set the following default values:
				String[] values 			= {"0","0","0","0", "0", "0", "0",  String.valueOf(confidencelevel*100 + "%"), "0 ; 0"};
				mathPanel.update(values);

				burninPanel.getValueLabel(0).setText(String.valueOf(numPoints));
				burninField.setText(String.valueOf(numPoints));
			}
		}
	}

	public MCMCDisplayPanel getFilePanel() 		{return filePanel;}
	public MCMCDisplayPanel getMathPanel() 		{return mathPanel;}
	public MCMCDisplayPanel getBurnInPanel() 	{return burninPanel;}
	@SuppressWarnings("rawtypes")
	public JComboBox getDropList() 				{return droplist;}
	public JTextField getBurnInField() 			{return burninField;}
	public MCMCGraphToolPanel getGraphTool() 	{return graphtoolPanel;}
	
	/* **************************************************************************** *
	 * 							END OF CLASS										*
	 * **************************************************************************** */
}
