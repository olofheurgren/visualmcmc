// $Id:
// forester -- software libraries and applications
// for genomics and evolutionary biology research.
//
// Copyright (C) 2010 Christian M Zmasek
// Copyright (C) 2010 Sanford-Burnham Medical Research Institute
// All rights reserved
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
//
// Contact: phylosoft @ gmail . com
// WWW: https://sites.google.com/site/cmzmasek/home/software/forester

package org.forester1.phylogeny.data;

import java.util.ArrayList;
import java.util.List;

import org.forester1.util.ForesterUtil;

public class Polygon implements PhylogenyData {

    private final List<Point> _points;

    public Polygon( final List<Point> points ) {
        _points = points;
    }

    @Override
    public StringBuffer asSimpleText() {
        final StringBuffer sb = new StringBuffer();
        boolean first = true;
        for( final Point point : getPoints() ) {
            if ( first ) {
                first = false;
            }
            else {
                sb.append( ForesterUtil.LINE_SEPARATOR );
            }
            sb.append( point.asSimpleText() );
        }
        return sb;
    }

    @Override
    public StringBuffer asText() {
        return asSimpleText();
    }

    @Override
    public PhylogenyData copy() {
        final List<Point> new_points = new ArrayList<Point>();
        for( final Point point : getPoints() ) {
            new_points.add( ( Point ) point.copy() );
        }
        return new Polygon( new_points );
    }

    public List<Point> getPoints() {
        return _points;
    }

    public boolean isEmpty() {
        return ForesterUtil.isEmpty( _points );
    }

    @Override
    public boolean isEqual( final PhylogenyData data ) {
        throw new UnsupportedOperationException();
    }

    @Override
    public StringBuffer toNHX() {
        throw new UnsupportedOperationException();
    }
}
