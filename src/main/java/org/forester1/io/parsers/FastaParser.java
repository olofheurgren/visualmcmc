// $Id:
//
// forester -- software libraries and applications
// for genomics and evolutionary biology research.
//
// Copyright (C) 2010 Christian M Zmasek
// Copyright (C) 2010 Sanford-Burnham Medical Research Institute
// All rights reserved
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
//
// Contact: phylosoft @ gmail . com
// WWW: https://sites.google.com/site/cmzmasek/home/software/forester

package org.forester1.io.parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FastaParser {

    private static final Pattern NAME_REGEX      = Pattern.compile( "^\\s*>\\s*(.+)" );
    private static final Pattern SEQ_REGEX       = Pattern.compile( "^\\s*(.+)" );
    private static final Pattern ANYTHING_REGEX  = Pattern.compile( "[\\d\\s]+" );
    //>gi|71834668|ref|NP_001025424.1| Bcl2 [Danio rerio]
    public static final Pattern  FASTA_DESC_LINE = Pattern
                                                         .compile( ">?\\s*([^|]+)\\|([^|]+)\\S*\\s+(.+)\\s+\\[(.+)\\]" );

    public static void main( final String[] args ) {
        final String a = ">gi|71834668|ref|NP_001025424.1| Bcl2 [Danio rerio]";
        final Matcher name_m = FASTA_DESC_LINE.matcher( a );
        if ( name_m.lookingAt() ) {
            System.out.println();
            System.out.println( name_m.group( 1 ) );
            System.out.println( name_m.group( 2 ) );
            System.out.println( name_m.group( 3 ) );
            System.out.println( name_m.group( 4 ) );
        }
        else {
            System.out.println( "Does not match." );
        }
    }

    static public boolean isLikelyFasta( final File f ) throws IOException {
        return isLikelyFasta( new FileInputStream( f ) );
    }

    static public boolean isLikelyFasta( final InputStream is ) throws IOException {
        final BufferedReader reader = new BufferedReader( new InputStreamReader( is, "UTF-8" ) );
        String line = null;
        while ( ( line = reader.readLine() ) != null ) {
            final boolean is_name_line = NAME_REGEX.matcher( line ).lookingAt();
            if ( canIgnore( line, true, false ) ) {
                continue;
            }
            else if ( is_name_line ) {
                reader.close();
                return true;
            }
            else if ( SEQ_REGEX.matcher( line ).lookingAt() ) {
                reader.close();
                return false;
            }
        }
        reader.close();
        return false;
    }

    static private boolean canIgnore( final String line, final boolean saw_first_seq, final boolean is_name_line ) {
        if ( ( line.length() < 1 ) || ANYTHING_REGEX.matcher( line ).matches() ) {
            return true;
        }
        if ( !saw_first_seq && !is_name_line ) {
            return true;
        }
        return false;
    }

}
